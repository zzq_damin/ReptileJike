使用须知：

一、该源码有两种功能：

1、一次性下载一门课程的所有视频，例如 J2EE 的所有课程视频；
2、挑自己所需的下载，例如只下载 J2EE 中的索引的基础操作的课程视频。

第1个功能如何使用：

操作data包中的Data类，
a、将select改为true；
b、将maxNum改为所需下载的课程视频的页数，如J2EE的视频页数是6
c、修改course为需要下载的课程链接，如J2EE的为http://www.jikexueyuan.com/course/j2ee/?pageNum=

第2个功能如何使用：

操作data包中的Data类，
a、将select改为false；
b、在courseInfo中按照实例格式录入所需视频数据；

二、下载极客视频需要用户的账户和密码，data包中的Data类中的username和password的vip的有效期是2016-10-18，过期之后需要修改为新的vip账户方可使用。

三、下载路径默认为H:/jike/，如果有需要可以在data包中的Data类的videPath自行修改。